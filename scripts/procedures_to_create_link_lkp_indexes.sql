USE data_vault;

DELIMITER $$

CREATE PROCEDURE data_vault.prc_create_dv_lkp_index_link (in p_table_name varchar(128))
BEGIN

SET @findIndex=CONCAT('SHOW INDEXES FROM ',database(),'.',p_table_name,' WHERE key_name = ''','ix_lkp_',p_table_name,'''');

select concat('create unique index ix_lkp_',table_name,' on ',database(),'.',table_name,'(',group_concat(column_name),')')
       as create_index_stmt
from
(
select table_name,ordering,column_name
from   (select table_name,0 as ordering,ordinal_position,column_name
        from   information_schema.columns
        where  table_name = p_table_name
        and    table_schema = (select database())
        and    column_name like 'hub%'
        UNION  ALL
        select table_name,1 as ordering,ordinal_position,column_name
        from   information_schema.columns
        where  table_name = p_table_name
        and    table_schema = (select database())
        and    column_key = 'PRI'
       ) column_names_to_sort
order by table_name,ordering,ordinal_position
) column_names
into @create_index_stmt;

PREPARE findIndex         FROM @findIndex;
PREPARE create_index_stmt FROM @create_index_stmt;
EXECUTE findIndex;
SET @found=FOUND_ROWS();
IF  NOT(@found > 0) THEN EXECUTE create_index_stmt; END IF;

DEALLOCATE PREPARE findIndex;
DEALLOCATE PREPARE create_index_stmt;

END;

$$

CREATE PROCEDURE data_vault.prc_create_dv_lkp_index_links_all()
BEGIN

DECLARE done INT DEFAULT 0;

DECLARE link_table_name VARCHAR(128);
DECLARE cur_table CURSOR FOR 
select table_name
from   information_schema.tables
where  table_name like 'link%'
and    table_name not like 'link%err'
and    table_schema = (select database())
order  by table_name;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

open cur_table;
read_loop: LOOP
    FETCH cur_table INTO link_table_name; 
    IF done THEN LEAVE read_loop; END IF;
    call data_vault.prc_create_dv_lkp_index_link(link_table_name);
  END LOOP;
  CLOSE cur_table;
END;