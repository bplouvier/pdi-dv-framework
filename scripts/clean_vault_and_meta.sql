use sakila_data_vault;

select concat('delete from ',table_name,';')
from information_schema.tables
where table_schema = DATABASE() 
and   table_name not like 'vw%'
order by case when table_name like 'hub%'  then 3
              when table_name like 'link%' then 2
              when table_name like 'sat%'  then 1
         end
         ,table_name

--Empty the Data Vault
delete from sat_actor;
delete from sat_actor_err;
delete from sat_address;
delete from sat_address_err;
delete from sat_category;
delete from sat_category_err;
delete from sat_city;
delete from sat_city_err;
delete from sat_country;
delete from sat_country_err;
delete from sat_customer;
delete from sat_customer_err;
delete from sat_film;
delete from sat_film_err;
delete from sat_language;
delete from sat_language_err;
delete from sat_link_film_head_actor_valid;
delete from sat_link_film_head_actor_valid_err;
delete from sat_payment;
delete from sat_payment_err;
delete from sat_rental;
delete from sat_rental_err;
delete from sat_staff;
delete from sat_staff_err;
delete from sat_staff_store_test;
delete from sat_staff_store_test_err;
delete from sat_store;
delete from sat_store_err;
delete from link_address_city;
delete from link_address_city_err;
delete from link_address_city_run;
delete from link_city_country;
delete from link_city_country_err;
delete from link_city_country_run;
delete from link_customer_address;
delete from link_customer_address_err;
delete from link_customer_address_run;
delete from link_customer_store;
delete from link_customer_store_err;
delete from link_customer_store_run;
delete from link_film_actor;
delete from link_film_actor_err;
delete from link_film_actor_run;
delete from link_film_category;
delete from link_film_category_err;
delete from link_film_category_run;
delete from link_film_head_actor;
delete from link_film_head_actor_err;
delete from link_film_head_actor_run;
delete from link_film_language;
delete from link_film_language_err;
delete from link_film_language_run;
delete from link_inventory;
delete from link_inventory_err;
delete from link_inventory_run;
delete from link_payment;
delete from link_payment_err;
delete from link_payment_rental;
delete from link_payment_rental_err;
delete from link_payment_rental_run;
delete from link_payment_run;
delete from link_rental;
delete from link_rental_err;
delete from link_rental_run;
delete from link_staff_address;
delete from link_staff_address_err;
delete from link_staff_address_run;
delete from link_staff_worksin_store;
delete from link_staff_worksin_store_err;
delete from link_staff_worksin_store_run;
delete from link_store_address;
delete from link_store_address_err;
delete from link_store_address_run;
delete from link_store_manager;
delete from link_store_manager_err;
delete from link_store_manager_run;
delete from hub_actor;
delete from hub_actor_err;
delete from hub_actor_talend;
delete from hub_address;
delete from hub_address_err;
delete from hub_category;
delete from hub_category_err;
delete from hub_city;
delete from hub_city_err;
delete from hub_country;
delete from hub_country_err;
delete from hub_customer;
delete from hub_customer_err;
delete from hub_film;
delete from hub_film_err;
delete from hub_inventory;
delete from hub_inventory_err;
delete from hub_language;
delete from hub_language_err;
delete from hub_payment;
delete from hub_payment_err;
delete from hub_rental;
delete from hub_rental_err;
delete from hub_staff;
delete from hub_staff_err;
delete from hub_store;
delete from hub_store_err;

select concat('alter table ',table_name,' AUTO_INCREMENT = 0;')
from information_schema.tables
where table_schema = DATABASE() 
and   table_name not like 'vw%'
and   table_name not like '%err'
order by case when table_name like 'hub%'  then 3
              when table_name like 'link%' then 2
              when table_name like 'sat%'  then 1
         end
         ,table_name



alter table sat_actor AUTO_INCREMENT = 0;
alter table sat_address AUTO_INCREMENT = 0;
alter table sat_category AUTO_INCREMENT = 0;
alter table sat_city AUTO_INCREMENT = 0;
alter table sat_country AUTO_INCREMENT = 0;
alter table sat_customer AUTO_INCREMENT = 0;
alter table sat_film AUTO_INCREMENT = 0;
alter table sat_language AUTO_INCREMENT = 0;
alter table sat_link_film_head_actor_valid AUTO_INCREMENT = 0;
alter table sat_payment AUTO_INCREMENT = 0;
alter table sat_rental AUTO_INCREMENT = 0;
alter table sat_staff AUTO_INCREMENT = 0;
alter table sat_staff_store_test AUTO_INCREMENT = 0;
alter table sat_store AUTO_INCREMENT = 0;
alter table link_address_city AUTO_INCREMENT = 0;
alter table link_address_city_run AUTO_INCREMENT = 0;
alter table link_city_country AUTO_INCREMENT = 0;
alter table link_city_country_run AUTO_INCREMENT = 0;
alter table link_customer_address AUTO_INCREMENT = 0;
alter table link_customer_address_run AUTO_INCREMENT = 0;
alter table link_customer_store AUTO_INCREMENT = 0;
alter table link_customer_store_run AUTO_INCREMENT = 0;
alter table link_film_actor AUTO_INCREMENT = 0;
alter table link_film_actor_run AUTO_INCREMENT = 0;
alter table link_film_category AUTO_INCREMENT = 0;
alter table link_film_category_run AUTO_INCREMENT = 0;
alter table link_film_head_actor AUTO_INCREMENT = 0;
alter table link_film_head_actor_run AUTO_INCREMENT = 0;
alter table link_film_language AUTO_INCREMENT = 0;
alter table link_film_language_run AUTO_INCREMENT = 0;
alter table link_inventory AUTO_INCREMENT = 0;
alter table link_inventory_run AUTO_INCREMENT = 0;
alter table link_payment AUTO_INCREMENT = 0;
alter table link_payment_rental AUTO_INCREMENT = 0;
alter table link_payment_rental_run AUTO_INCREMENT = 0;
alter table link_payment_run AUTO_INCREMENT = 0;
alter table link_rental AUTO_INCREMENT = 0;
alter table link_rental_run AUTO_INCREMENT = 0;
alter table link_staff_address AUTO_INCREMENT = 0;
alter table link_staff_address_run AUTO_INCREMENT = 0;
alter table link_staff_worksin_store AUTO_INCREMENT = 0;
alter table link_staff_worksin_store_run AUTO_INCREMENT = 0;
alter table link_store_address AUTO_INCREMENT = 0;
alter table link_store_address_run AUTO_INCREMENT = 0;
alter table link_store_manager AUTO_INCREMENT = 0;
alter table link_store_manager_run AUTO_INCREMENT = 0;
alter table hub_actor AUTO_INCREMENT = 0;
alter table hub_actor_talend AUTO_INCREMENT = 0;
alter table hub_address AUTO_INCREMENT = 0;
alter table hub_category AUTO_INCREMENT = 0;
alter table hub_city AUTO_INCREMENT = 0;
alter table hub_country AUTO_INCREMENT = 0;
alter table hub_customer AUTO_INCREMENT = 0;
alter table hub_film AUTO_INCREMENT = 0;
alter table hub_inventory AUTO_INCREMENT = 0;
alter table hub_language AUTO_INCREMENT = 0;
alter table hub_payment AUTO_INCREMENT = 0;
alter table hub_rental AUTO_INCREMENT = 0;
alter table hub_staff AUTO_INCREMENT = 0;
alter table hub_store AUTO_INCREMENT = 0;

use pdi_meta_dv_demo;

--Empty PDI_META

delete from etl_log_channel;
delete from etl_log_job_entry;
delete from etl_log_job;
delete from etl_log_transformation;
delete from etl_log_transformation_step;
delete from etl_log_transformation_step_performance;

delete from inst_run_dv_jobs;
delete from inst_run_parameters;
delete from inst_run_source_files;
delete from inst_run_transformations;
delete from inst_runs;

delete from ref_data_vault_link_satellites;
delete from ref_data_vault_link_satellites_hist;
delete from ref_data_vault_link_attributes;
delete from ref_data_vault_link_attributes_hist;
delete from ref_data_vault_link_sources;
delete from ref_data_vault_link_sources_hist;
delete from ref_data_vault_links;
delete from ref_data_vault_links_hist;
delete from ref_data_vault_hub_satellites;
delete from ref_data_vault_hub_satellites_hist;
delete from ref_data_vault_hub_sources;
delete from ref_data_vault_hub_sources_hist;
delete from ref_data_vault_hubs;
delete from ref_data_vault_hubs_hist;
delete from ref_data_vaults;
delete from ref_data_vaults_hist;
delete from ref_source_tables_hist;
delete from ref_source_tables;
delete from ref_source_systems_hist;
delete from ref_source_systems;

alter table  ref_data_vault_link_satellites  AUTO_INCREMENT = 0;
alter table  ref_data_vault_link_attributes  AUTO_INCREMENT = 0;
alter table  ref_data_vault_links            AUTO_INCREMENT = 0;
alter table  ref_data_vault_hub_satellites   AUTO_INCREMENT = 0;
alter table  ref_data_vault_hubs             AUTO_INCREMENT = 0;
alter table  ref_source_tables               AUTO_INCREMENT = 0;

