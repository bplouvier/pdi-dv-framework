<transformation>
  <info>
    <name>trf_data_vault_determine_hubs_to_load</name>
    <description>Determine hubs to load</description>
    <extended_description>This transformation collects all necessary parameters to load the hubs.
Each combination of a hub an source table results in 1 row.
Those rows are placed in a &apos;result set&apos;, a job to load the hubs picks up those rows and gets executed once for each row.

History

Version     When           Who                What
1.0         2011-03-01     Edwin Weber        Initial version  
1.1         2011-05-27     Edwin Weber        Added selection of hub_source_order.
1.2         2012-07-04     Edwin Weber        Added selection of ind_last_seen_dts.
1.3         2013-03-01     Edwin Weber        Added selection of ind_status_sat.</extended_description>
    <trans_version>1.3</trans_version>
    <trans_type>Normal</trans_type>
    <trans_status>2</trans_status>
    <directory>&#47;data_vault</directory>
    <parameters>
        <parameter>
            <name>id_data_vault</name>
            <default_value/>
            <description/>
        </parameter>
        <parameter>
            <name>mod_4_value</name>
            <default_value/>
            <description/>
        </parameter>
    </parameters>
    <log>
<trans-log-table><connection/>
<schema/>
<table/>
<size_limit_lines/>
<interval/>
<timeout_days/>
<field><id>ID_BATCH</id><enabled>Y</enabled><name>ID_BATCH</name></field><field><id>CHANNEL_ID</id><enabled>Y</enabled><name>CHANNEL_ID</name></field><field><id>TRANSNAME</id><enabled>Y</enabled><name>TRANSNAME</name></field><field><id>STATUS</id><enabled>Y</enabled><name>STATUS</name></field><field><id>LINES_READ</id><enabled>Y</enabled><name>LINES_READ</name><subject/></field><field><id>LINES_WRITTEN</id><enabled>Y</enabled><name>LINES_WRITTEN</name><subject/></field><field><id>LINES_UPDATED</id><enabled>Y</enabled><name>LINES_UPDATED</name><subject/></field><field><id>LINES_INPUT</id><enabled>Y</enabled><name>LINES_INPUT</name><subject/></field><field><id>LINES_OUTPUT</id><enabled>Y</enabled><name>LINES_OUTPUT</name><subject/></field><field><id>LINES_REJECTED</id><enabled>Y</enabled><name>LINES_REJECTED</name><subject/></field><field><id>ERRORS</id><enabled>Y</enabled><name>ERRORS</name></field><field><id>STARTDATE</id><enabled>Y</enabled><name>STARTDATE</name></field><field><id>ENDDATE</id><enabled>Y</enabled><name>ENDDATE</name></field><field><id>LOGDATE</id><enabled>Y</enabled><name>LOGDATE</name></field><field><id>DEPDATE</id><enabled>Y</enabled><name>DEPDATE</name></field><field><id>REPLAYDATE</id><enabled>Y</enabled><name>REPLAYDATE</name></field><field><id>LOG_FIELD</id><enabled>Y</enabled><name>LOG_FIELD</name></field></trans-log-table>
<perf-log-table><connection/>
<schema/>
<table/>
<interval/>
<timeout_days/>
<field><id>ID_BATCH</id><enabled>Y</enabled><name>ID_BATCH</name></field><field><id>SEQ_NR</id><enabled>Y</enabled><name>SEQ_NR</name></field><field><id>LOGDATE</id><enabled>Y</enabled><name>LOGDATE</name></field><field><id>TRANSNAME</id><enabled>Y</enabled><name>TRANSNAME</name></field><field><id>STEPNAME</id><enabled>Y</enabled><name>STEPNAME</name></field><field><id>STEP_COPY</id><enabled>Y</enabled><name>STEP_COPY</name></field><field><id>LINES_READ</id><enabled>Y</enabled><name>LINES_READ</name></field><field><id>LINES_WRITTEN</id><enabled>Y</enabled><name>LINES_WRITTEN</name></field><field><id>LINES_UPDATED</id><enabled>Y</enabled><name>LINES_UPDATED</name></field><field><id>LINES_INPUT</id><enabled>Y</enabled><name>LINES_INPUT</name></field><field><id>LINES_OUTPUT</id><enabled>Y</enabled><name>LINES_OUTPUT</name></field><field><id>LINES_REJECTED</id><enabled>Y</enabled><name>LINES_REJECTED</name></field><field><id>ERRORS</id><enabled>Y</enabled><name>ERRORS</name></field><field><id>INPUT_BUFFER_ROWS</id><enabled>Y</enabled><name>INPUT_BUFFER_ROWS</name></field><field><id>OUTPUT_BUFFER_ROWS</id><enabled>Y</enabled><name>OUTPUT_BUFFER_ROWS</name></field></perf-log-table>
<channel-log-table><connection/>
<schema/>
<table/>
<timeout_days/>
<field><id>ID_BATCH</id><enabled>Y</enabled><name>ID_BATCH</name></field><field><id>CHANNEL_ID</id><enabled>Y</enabled><name>CHANNEL_ID</name></field><field><id>LOG_DATE</id><enabled>Y</enabled><name>LOG_DATE</name></field><field><id>LOGGING_OBJECT_TYPE</id><enabled>Y</enabled><name>LOGGING_OBJECT_TYPE</name></field><field><id>OBJECT_NAME</id><enabled>Y</enabled><name>OBJECT_NAME</name></field><field><id>OBJECT_COPY</id><enabled>Y</enabled><name>OBJECT_COPY</name></field><field><id>REPOSITORY_DIRECTORY</id><enabled>Y</enabled><name>REPOSITORY_DIRECTORY</name></field><field><id>FILENAME</id><enabled>Y</enabled><name>FILENAME</name></field><field><id>OBJECT_ID</id><enabled>Y</enabled><name>OBJECT_ID</name></field><field><id>OBJECT_REVISION</id><enabled>Y</enabled><name>OBJECT_REVISION</name></field><field><id>PARENT_CHANNEL_ID</id><enabled>Y</enabled><name>PARENT_CHANNEL_ID</name></field><field><id>ROOT_CHANNEL_ID</id><enabled>Y</enabled><name>ROOT_CHANNEL_ID</name></field></channel-log-table>
<step-log-table><connection/>
<schema/>
<table/>
<timeout_days/>
<field><id>ID_BATCH</id><enabled>Y</enabled><name>ID_BATCH</name></field><field><id>CHANNEL_ID</id><enabled>Y</enabled><name>CHANNEL_ID</name></field><field><id>LOG_DATE</id><enabled>Y</enabled><name>LOG_DATE</name></field><field><id>TRANSNAME</id><enabled>Y</enabled><name>TRANSNAME</name></field><field><id>STEPNAME</id><enabled>Y</enabled><name>STEPNAME</name></field><field><id>STEP_COPY</id><enabled>Y</enabled><name>STEP_COPY</name></field><field><id>LINES_READ</id><enabled>Y</enabled><name>LINES_READ</name></field><field><id>LINES_WRITTEN</id><enabled>Y</enabled><name>LINES_WRITTEN</name></field><field><id>LINES_UPDATED</id><enabled>Y</enabled><name>LINES_UPDATED</name></field><field><id>LINES_INPUT</id><enabled>Y</enabled><name>LINES_INPUT</name></field><field><id>LINES_OUTPUT</id><enabled>Y</enabled><name>LINES_OUTPUT</name></field><field><id>LINES_REJECTED</id><enabled>Y</enabled><name>LINES_REJECTED</name></field><field><id>ERRORS</id><enabled>Y</enabled><name>ERRORS</name></field><field><id>LOG_FIELD</id><enabled>N</enabled><name>LOG_FIELD</name></field></step-log-table>
    </log>
    <maxdate>
      <connection/>
      <table/>
      <field/>
      <offset>0.0</offset>
      <maxdiff>0.0</maxdiff>
    </maxdate>
    <size_rowset>10000</size_rowset>
    <sleep_time_empty>50</sleep_time_empty>
    <sleep_time_full>50</sleep_time_full>
    <unique_connections>N</unique_connections>
    <feedback_shown>Y</feedback_shown>
    <feedback_size>50000</feedback_size>
    <using_thread_priorities>Y</using_thread_priorities>
    <shared_objects_file/>
    <capture_step_performance>N</capture_step_performance>
    <step_performance_capturing_delay>1000</step_performance_capturing_delay>
    <step_performance_capturing_size_limit>100</step_performance_capturing_size_limit>
    <dependencies>
    </dependencies>
    <partitionschemas>
    </partitionschemas>
    <slaveservers>
    </slaveservers>
    <clusterschemas>
    </clusterschemas>
  <created_user>admin</created_user>
  <created_date>2011&#47;02&#47;02 21:09:36.000</created_date>
  <modified_user>admin</modified_user>
  <modified_date>2013&#47;03&#47;03 16:42:55.026</modified_date>
  </info>
  <notepads>
    <notepad>
      <note>Description: see transformation settings</note>
      <xloc>43</xloc>
      <yloc>20</yloc>
      <width>224</width>
      <heigth>24</heigth>
      <fontname>Arial</fontname>
      <fontsize>9</fontsize>
      <fontbold>N</fontbold>
      <fontitalic>N</fontitalic>
      <fontcolorred>0</fontcolorred>
      <fontcolorgreen>0</fontcolorgreen>
      <fontcolorblue>0</fontcolorblue>
      <backgroundcolorred>255</backgroundcolorred>
      <backgroundcolorgreen>255</backgroundcolorgreen>
      <backgroundcolorblue>0</backgroundcolorblue>
      <bordercolorred>100</bordercolorred>
      <bordercolorgreen>100</bordercolorgreen>
      <bordercolorblue>100</bordercolorblue>
      <drawshadow>Y</drawshadow>
    </notepad>
  </notepads>
  <connection>
    <name>pdi_logging</name>
    <server>localhost</server>
    <type>MYSQL</type>
    <access>Native</access>
    <database>pdi_meta_dv_demo</database>
    <port>3306</port>
    <username>root</username>
    <password>Encrypted 2be98afc86aa7f2e4cb79b975dc81a0d6</password>
    <servername/>
    <data_tablespace/>
    <index_tablespace/>
    <attributes>
      <attribute><code>EXTRA_OPTION_MYSQL.AUTOINCREMENT_SQL_FOR_BATCH_ID</code><attribute>UPDATE etl_log_counter SET ID=LAST_INSERT_ID(ID+1)</attribute></attribute>
      <attribute><code>EXTRA_OPTION_MYSQL.defaultFetchSize</code><attribute>500</attribute></attribute>
      <attribute><code>EXTRA_OPTION_MYSQL.supports boolean data type</code><attribute>true</attribute></attribute>
      <attribute><code>EXTRA_OPTION_MYSQL.useCursorFetch</code><attribute>false</attribute></attribute>
      <attribute><code>EXTRA_OPTION_MYSQL.useServerPrepStmts</code><attribute>false</attribute></attribute>
      <attribute><code>EXTRA_OPTION_MYSQL.zeroDateBehavior</code><attribute>convertToNull</attribute></attribute>
      <attribute><code>EXTRA_OPTION_MYSQL.zeroDateTimeBehavior</code><attribute>convertToNull</attribute></attribute>
      <attribute><code>FORCE_IDENTIFIERS_TO_LOWERCASE</code><attribute>N</attribute></attribute>
      <attribute><code>FORCE_IDENTIFIERS_TO_UPPERCASE</code><attribute>N</attribute></attribute>
      <attribute><code>IS_CLUSTERED</code><attribute>N</attribute></attribute>
      <attribute><code>PORT_NUMBER</code><attribute>3306</attribute></attribute>
      <attribute><code>QUOTE_ALL_FIELDS</code><attribute>N</attribute></attribute>
      <attribute><code>STREAM_RESULTS</code><attribute>N</attribute></attribute>
      <attribute><code>SUPPORTS_BOOLEAN_DATA_TYPE</code><attribute>N</attribute></attribute>
      <attribute><code>USE_POOLING</code><attribute>N</attribute></attribute>
    </attributes>
  </connection>
  <connection>
    <name>pdi_meta</name>
    <server>localhost</server>
    <type>MYSQL</type>
    <access>Native</access>
    <database>pdi_meta_dv_demo</database>
    <port>3306</port>
    <username>root</username>
    <password>Encrypted 2be98afc86aa7f2e4cb79b975dc81a0d6</password>
    <servername/>
    <data_tablespace/>
    <index_tablespace/>
    <attributes>
      <attribute><code>EXTRA_OPTION_MYSQL.defaultFetchSize</code><attribute>500</attribute></attribute>
      <attribute><code>EXTRA_OPTION_MYSQL.supports boolean data type</code><attribute>true</attribute></attribute>
      <attribute><code>EXTRA_OPTION_MYSQL.useCursorFetch</code><attribute>false</attribute></attribute>
      <attribute><code>EXTRA_OPTION_MYSQL.zeroDateBehavior</code><attribute>convertToNull</attribute></attribute>
      <attribute><code>EXTRA_OPTION_MYSQL.zeroDateTimeBehavior</code><attribute>convertToNull</attribute></attribute>
      <attribute><code>FORCE_IDENTIFIERS_TO_LOWERCASE</code><attribute>N</attribute></attribute>
      <attribute><code>FORCE_IDENTIFIERS_TO_UPPERCASE</code><attribute>N</attribute></attribute>
      <attribute><code>IS_CLUSTERED</code><attribute>N</attribute></attribute>
      <attribute><code>PORT_NUMBER</code><attribute>3306</attribute></attribute>
      <attribute><code>QUOTE_ALL_FIELDS</code><attribute>N</attribute></attribute>
      <attribute><code>STREAM_RESULTS</code><attribute>N</attribute></attribute>
      <attribute><code>SUPPORTS_BOOLEAN_DATA_TYPE</code><attribute>N</attribute></attribute>
      <attribute><code>USE_POOLING</code><attribute>N</attribute></attribute>
    </attributes>
  </connection>
  <order>
  <hop> <from>ref_data_vault_hub_sources</from><to>Copy rows to result</to><enabled>Y</enabled> </hop>  </order>
  <step>
    <name>Copy rows to result</name>
    <type>RowsToResult</type>
    <description/>
    <distribute>Y</distribute>
    <copies>1</copies>
         <partitioning>
           <method>none</method>
           <schema_name/>
           </partitioning>
     <cluster_schema/>
 <remotesteps>   <input>   </input>   <output>   </output> </remotesteps>    <GUI>
      <xloc>320</xloc>
      <yloc>96</yloc>
      <draw>Y</draw>
      </GUI>
    </step>

  <step>
    <name>ref_data_vault_hub_sources</name>
    <type>TableInput</type>
    <description/>
    <distribute>Y</distribute>
    <copies>1</copies>
         <partitioning>
           <method>none</method>
           <schema_name/>
           </partitioning>
    <connection>pdi_meta</connection>
    <sql>select     hubs.hub_name
,          hubs.hub_key
,          hubs.hub_business_key
,          ifnull(srctab.staging_table_name,srctab.table_name)              as hub_source
,          src.source_order                                                 as hub_source_order
,          src.source_business_key                                          as hub_source_business_key
,          src.record_source_id                                             as hub_source_record_source_id
,          ifnull(srcsys.id_staging_connection,srcsys.id_source_connection) as id_connection
,          case when conn.type in (&apos;MySQL&apos;) then &apos;trf_data_vault_hub_generic&apos; 
                when conn.type in (&apos;MSSQL&apos;) then &apos;trf_data_vault_hub_generic_src_sqlserver&apos;
           end                                                              as hub_transformation_to_execute
,          hubs.ind_last_seen_dts
,          src.ind_status_sat
from       ref_data_vault_hubs        hubs
inner join ref_data_vault_hub_sources src
on         hubs.id_data_vault_hub   = src.id_data_vault_hub
inner join ref_source_tables          srctab
on         src.record_source_id     = srctab.id_srctab
inner join ref_source_systems         srcsys
on         srctab.id_srcsys         = srcsys.id_srcsys
inner join ref_connections               conn
on         ifnull(srcsys.id_staging_connection,srcsys.id_source_connection) = conn.id_connection
where      hubs.ind_current         = 1
and        src.ind_current          = 1
and        mod(hubs.id_data_vault_hub,4) = ${mod_4_value}
and        hubs.id_data_vault            = ${id_data_vault}
order by   hubs.hub_name
,          src.source_order</sql>
    <limit>0</limit>
    <lookup/>
    <execute_each_row>N</execute_each_row>
    <variables_active>Y</variables_active>
    <lazy_conversion_active>N</lazy_conversion_active>
     <cluster_schema/>
 <remotesteps>   <input>   </input>   <output>   </output> </remotesteps>    <GUI>
      <xloc>96</xloc>
      <yloc>96</yloc>
      <draw>Y</draw>
      </GUI>
    </step>

  <step_error_handling>
  </step_error_handling>
   <slave-step-copy-partition-distribution>
</slave-step-copy-partition-distribution>
   <slave_transformation>N</slave_transformation>
</transformation>
